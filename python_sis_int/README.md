Este flake fue creado específicamente para la clase de _Sistemas Inteligentes_.
Cuenta con los siguientes paquetes para python 12:

1. `pandas`
2. `matplotlib`
3. `seaborn`
4. `pyqt6`
5. `numpy`
6. `scikit-learn`
7. `opencv4`
