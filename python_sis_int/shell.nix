{ pkgs ? import <nixpkgs> { } }:
with pkgs;
mkShell {
  buildInputs = [
    (pkgs.python3.withPackages (python-pkgs: [
      python-pkgs.pandas
      python-pkgs.matplotlib
      python-pkgs.seaborn
      python-pkgs.pyqt6
      python-pkgs.numpy
      python-pkgs.scikit-learn
      python-pkgs.opencv4
    ]))
  ];

  shellHook = ''
    tmux
  '';
}
